## Installation
You can use yarn or npm to install project dependencies. Yarn is included in the project dependencies so you won't need to install it globally.

```
npm install
```

```
yarn install
```

## Running the app
### Environment Variables
We want to keep secrets out of source control. You will need to setup an new app in your Twitter account https://apps.twitter.com/ and provide the following 4 environment variables, via `.env` file, in order to connect to twitter. There is a `sample.env` file for reference.

```
TWITTER_CONSUMER_KEY=your_key
TWITTER_CONSUMER_SECRET=your_secret
TWITTER_ACCESS_TOKEN=your_token
TWITTER_ACCESS_SECRET=your_secret
```

If you have a preference to run on a different port you can add that as well, the default to `4000`.
```
BACKEND_PORT=4000
```

### Starting
You can run `yarn start` which will run both the frontend and backend. The downside is you will not get output for either the frontend or the backend depending on which one starts up first.

You may choose to run the frontend separately from the backend. `yarn start-js` will run the frontend and `yarn start-backend` will run the backend. Make sure to do these in separate shells.