export const TweetCountOptions = [
  {
    label: '10',
    value: 10
  },
  {
    label: '25',
    value: 25
  },
  {
    label: '50',
    value: 50
  },
  {
    label: '75',
    value: 75
  },
  {
    label: '100',
    value: 100
  }
];

export const SimpleOperators = [
  {
    label: 'Mentioned',
    value: 'mentioned'
  },
  {
    label: 'From',
    value: 'from'
  },
  {
    label: 'To',
    value: 'to'
  },
];

// export const TweetCountOptions = [{

// }];
