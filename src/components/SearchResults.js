import React from 'react';

export default (props) => {
  if (props.wasSearched && !props.tweets.length) {
    return 'No tweets found';
  }

  return (
    <ul className="no-bullet tweet-list">
      {props.tweets.map(tweet => {
        return (
          <li key={tweet.id_str}>
            <div className="tweet-content">{tweet.text}</div>
            <span>@{tweet.user.screen_name}</span>
          </li>
        );
      })}
    </ul>
  );
}
