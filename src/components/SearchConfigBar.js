import React from 'react';
import { SimpleDropdown } from './';
import { TweetCountOptions, SimpleOperators } from '../data/static';

export default props => {
  return (
    <div className="search-config-bar">
      <div className="grid-x">
        <div className="medium-auto cell">
          <SimpleDropdown
            data={TweetCountOptions}
            defaultValue={props.defaultQueryValues.count}
            label="Count"
            name="count"
            onChange={props.updateSearchQuery}
          />
        </div>
        <div className="medium-shrink cell">
          <SimpleDropdown
            data={SimpleOperators}
            defaultValue={props.defaultQueryValues.operator}
            label="Operator"
            name="operator"
            onChange={props.updateSearchQuery}
          />
        </div>
      </div>
    </div>
  );
}
