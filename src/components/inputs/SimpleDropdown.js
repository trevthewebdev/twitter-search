import React, { Component } from 'react';
import classnames from 'classnames';

const Option = (props) => {
  return (
    <li
      onClick={() => {
        props.handleSelection(props.data.value);
        props.toggleShow();
      }}
    >
      {props.data.label}
    </li>
  );
}

class SimpleDropDown extends Component {
  constructor(props) {
    super();

    this.state = {
      current: props.defaultValue || null,
      isOpen: false
    };
  }

  render() {
    const { props, state } = this;
    const className = classnames('dropdown', { 'is-open': state.isOpen });
    let fieldLabel = '-Select-';

    if (state.current) {
      fieldLabel = props.data.find(option => option.value === state.current).label;
    }

    return (
      <div className={className}>
        <span className="dropdown__label">{props.label}</span>
        <button onClick={this.toggleShow}>{fieldLabel}</button>
        <ul className="no-bullet">
          {props.data.map(option => {
            return (
              <Option
                data={option}
                handleSelection={this.handleSelection}
                key={option.value}
                toggleShow={this.toggleShow}
              />
            );
          })}
        </ul>
      </div>
    );
  }

  handleSelection = (value) => {
    const { props } = this;
    const query = {};

    query[props.name] = value;
    this.setState({ current: value });

    if (props.onChange) {
      props.onChange(query);
    }
  }

  toggleShow = () => {
    this.setState({ isOpen: !this.state.isOpen });
  }
}

export default SimpleDropDown;
