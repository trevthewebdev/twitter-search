import React from 'react';

export default (props) => {
  if (!props.errors.length) {
    return '';
  }

  return (
    <div className="container errors-list">
      <div className="grid-x align-center">
        <div className="medium-6 cell">
          <ul className="no-bullet">
            {props.errors.map((error, index) => {
              return (
                <li key={`error-${index}`}>{error.text}</li>
              );
            })}
          </ul>
        </div>
      </div>
    </div>
  );
}
