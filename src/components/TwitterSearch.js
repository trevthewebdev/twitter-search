import React, { PureComponent } from 'react';

class TwitterSearch extends PureComponent {
  render() {
    const { props } = this;

    return (
      <form onSubmit={props.handleSearch}>
        <div className="input-group">
          <span className="input-group-label">@</span>
          <input
            className="input-group-field"
            onInput={(event) => {
              props.updateSearchQuery({ handle: event.target.value });
            }}
            placeholder="thedicetower"
            type="text"
          />
          <div className="input-group-button">
            <input type="submit" className="button" value="Search" />
          </div>
        </div>
      </form>
    );
  }
}

export default TwitterSearch;
