import React, { Component } from 'react';
import Request from 'axios';
import moment from 'moment';

import './App.css';
import ErrorBar from './components/ErrorBar';
import TwitterSearch from './components/TwitterSearch';
import SearchResults from './components/SearchResults';
import SearchConfigBar from './components/SearchConfigBar';

class App extends Component {
  constructor() {
    super();

    this.state = {
      errors: [],
      query: {
        count: 10,
        operator: 'mentioned'
      },
      tweets: [],
      wasSearched: false
    };
  }

  render() {
    const { state } = this;
    const earliestTweetDate = moment().subtract(7, "days").format("MMMM Do, YYYY");

    return (
      <div className="container twitter-container">
        <ErrorBar errors={state.errors} />
        <div className="grid-x align-center">
          <div className="medium-6 cell">
            <p>Generic tweet search with some configuration. You can search for tweets as far back as <strong>{earliestTweetDate}</strong></p>
            <SearchConfigBar
              defaultQueryValues={state.query}
              updateSearchQuery={this.updateSearchQuery}
            />
            <TwitterSearch
              handleSearch={this.handleSearch}
              updateSearchQuery={this.updateSearchQuery}
            />
            <SearchResults
              tweets={state.tweets}
              wasSearched={state.wasSearched}
            />
          </div>
        </div>
      </div>
    );
  }

  handleSearch = event => {
    event.preventDefault();

    Request
      .get('http://localhost:4000/api/tweets', {
        params: this.state.query
      })
      .then((res) => {
        // was successful set the tweets
        this.setState({
          errors: [],
          tweets: res.data.statuses,
          wasSearched: true
        });
      })
      .catch(error => {
        const { response } = error;
        if (response && response.status == 400) {
          this.setState({
            errors: [{
              text: 'You forgot to provide a handle to search for'
            }]
          });
        } else {
          this.setState({
            errors: [{
              text: `Looks like we're having some issues, please try again later`
            }]
          });
        }
      });
  }

  updateSearchQuery = params => {
    const { state } = this;

    this.setState({
      query: Object.assign({}, state.query, params)
    });
  }
}

export default App;
