import dotenv from 'dotenv';
dotenv.load();

export default {
  PORT:                     process.env.BACKEND_PORT || 4000,
  HOSTNAME:                 process.env.BACKEND_HOSTNAME || 'localhost',
  TWITTER_CONSUMER_KEY:     process.env.TWITTER_CONSUMER_KEY,
  TWITTER_CONSUMER_SECRET:  process.env.TWITTER_CONSUMER_SECRET,
  TWITTER_ACCESS_TOKEN:     process.env.TWITTER_ACCESS_TOKEN,
  TWITTER_ACCESS_SECRET:    process.env.TWITTER_ACCESS_SECRET
};
