import Twitter from 'twitter';
import config from './config';

export const API_BASE_PATH = 'https://api.twitter.com/1.1';

export default new Twitter({
  consumer_key: config.TWITTER_CONSUMER_KEY,
  consumer_secret: config.TWITTER_CONSUMER_SECRET,
  access_token_key: config.TWITTER_ACCESS_TOKEN,
  access_token_secret: config.TWITTER_ACCESS_SECRET
});
