import Express from 'express';
import TwitterClient, { API_BASE_PATH } from './twitterClient';

const Router = Express.Router();

Router.get('/tweets', (req, res) => {
  const { query } = req;
  const queryKeys = Object.keys(query);
  let queryString = 'q=';

  // Bad request if user didn't provide the handle to search for
  if (!query.handle) {
    return res.status(400).json({
      errors: [{
        status: '400',
        title: 'Invalid Request',
        detail: 'No handle specified'
      }]
    });
  }

  // A subset of twitter's operators
  switch(query.operator) {
    case 'from':
    case 'to':
      queryString += `${query.operator}:`;
      break;

    default:
      queryString += '@';
  }

  // handle is the first thing that comes after operator
  queryString += query.handle.replace('@', '');

  // append the rest of the query params
  queryKeys.forEach(function(key) {
    if (key !== 'handle') {
      queryString += `&${key}=${query[key]}`;
    }
  });

  TwitterClient
    .get(`${API_BASE_PATH}/search/tweets.json?${queryString}`, {})
    .then(tweets => {
      res.json(tweets);
    })
    .catch(errors => {
      if (Array.isArray(errors) && errors[0].code === 32) {
        return res.status(401).json({
          errors: [{
            status: '401',
            title: 'Unauthorized'
          }]
        });
      }
      res.json(error);
    });
});

export default Router;
