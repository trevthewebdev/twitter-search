import bodyParser from "body-parser";

function useCors(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
}

export default (App) => {
  App.use(useCors);
  App.use(bodyParser.urlencoded({ extended: true }));
  App.use(bodyParser.json());
}
