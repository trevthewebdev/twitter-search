import config from './config';
import Express from 'express';
import BodyParser from 'body-parser';
import routes from './routes';
import middleware from './middleware';

const App = Express();

middleware(App);

App.use('/api', routes);

App.listen(config.PORT, () => console.log(`Server Listening on http://${config.HOSTNAME}:${config.PORT}`));
